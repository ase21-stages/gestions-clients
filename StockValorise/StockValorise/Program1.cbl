       program-id. Program1 as "StockValorise.Program1".

       environment division.
       input-output section.

                     select f-valeurstock assign to "C:\Users\Quentin\Desktop\StockValorise.txt"
                            status StatusCode organization Line Sequential access sequential.
       data division.
       file section.
       fd f-valeurstock record varying from 0 to 255.
       01 e-valeurstock pic x(255).

       
       working-storage section.

           77 CNXDB STRING.
               EXEC SQL
                   INCLUDE SQLCA
               END-EXEC.

               EXEC SQL
                   INCLUDE SQLDA
               END-EXEC.

           01 DateSysteme.
             10 Annee Pic 99.
             10 Mois Pic 99.
             10 Jour Pic 99.

           01 HeureSysteme.
             02 Heure PIC 99.
             02 Minute PIC 99.
             02 FILLER PIC 9(4).



           01 Article.
             10 Ref sql char (10).
             10 Nom sql char-varying (30).
             10 Stock pic 9(10).
             10 StockMini pic 9(10).
             10 Prix pic 9(5)V99.
             10 totU pic 9(11).




           01 articleImpr.
             10 Ref pic x(10).
             10 Nom pic x(30).
             10 Stock pic z(9)9.
             10 StockMini pic z(9)9.
             10 Prix pic z(4)9V99.
             10 totU pic z(10)9.


           77 prixTotal pic 9(11).
           77 pass pic 9 value 1.
           77 eob pic 9.
           77 NbLigne pic 999.
           77 MaxLigneEtat pic 999.
           77 NoPage pic 999.

       01 totAff.
         10 filler pic x(50).
         10 filler pic x(16) value "Stock valoris� : ".
         10 tot pic z(10)9.
          

       01 LigneEntete.
         10 Filler Pic X(17) value "Mis � jour le : ".
         10 jour pic 99.
         10 Filler Pic X value "/".
         10 mois pic 99.
         10 Filler Pic X value "/".
         10 Annee Pic 99.
         10 filler pic xxxx.
         10 filler pic x(3) value "A  ".
         10 Heure pic 99.
         10 filler pic x value ":".
         10 Minute pic 99.

       

       01 LigneEntete2.
         10 Filler Pic X(70).

       01 LigneEntete3.
         10 Filler Pic X(120) value all "=".
      
       01 LigneEntete4.
         10 filler pic x.
         10 filler pic X(12) value "Reference ||".
         10 filler pic x(5).
         10 filler pic x(10) value "Stock ||".
         10 filler pic x(5).
         10 filler pic x(10) value "Prix ||".
         10 filler pic x(5).
         10 filler pic x(17) value "Total unitaire ||".


       01 LigneArticle.
      *  10 filler pic x.
         10 ref pic 9(10).
         10 filler pic x(5).
         10 stock pic z(9)9 .
         10 filler pic x(5).
         10 prix pic z(4)9V,99.
         10 filler pic x(5).
         10 totU pic z(10)9.

       


       01 DernierBasPage.
         10 Filler Pic X(10) value " --- Page ".
         10 NPage Pic Z9.
         10 Filler Pic X.
         10 Filler Pic X(97) value all "-".

       01 LigneBasPage.
         10 Filler Pic X(10) value " --- Page ".
         10 NPage Pic Z9.
         10 Filler Pic X.
         10 Filler Pic X(85) value all "-".
         10 Filler Pic X(13) value " A suivre ---".


        77 StatusCode pic 99.
        77 ligneImp pic 9(5).

       procedure division.

           accept DateSysteme from date.
           accept HeureSysteme from time.
       
       

           MOVE
             "Trusted_Connection=yes;Database=GestionClients;server=DESKTOP-HQ-A-W\SQLEXPRESS;factory=System.Data.SqlClient;"
             to cnxDb.
           exec sql
               connect using :cnxDb
           end-exec.

           if (sqlcode not equal 0) then
               stop run
           end-if.

           exec sql
               SET AUTOCOMMIT ON
           end-exec.



       CreationFichier.
           perform init.
           perform trt until eob = 1.
           perform fin.

       init.
           exec sql 
              declare curs-b cursor 
              for 
              select ref, nom, prix, Stock 
              from stock 
           end-exec.

           exec sql 
               open curs-b
           end-exec.

           move 1 to pass.
           move 0 to eob.

           move 0 to ligneImp.
           
           

       trt.
           exec sql 
              fetch curs-b into :article.ref, :article.nom , :article.prix , :article.stock
           end-exec.
           
           if (sqlcode = 100 and pass = 1)
               display "erreur aucune ligne dans bdd"
               move 1 to eob
           

           else if (sqlcode =0 or sqlcode =1)
            move stock of Article to totU of article
            multiply totU of article by prix of article giving totu of article
            add totU of article to prixTotal 
            add 1 to MaxLigneEtat
            perform verImpr
            move 0 to pass

           else 
               move 1 to eob

           end-if.


       fin.
           write e-valeurstock from DernierBasPage.
           move prixTotal to tot of totAff.
           write e-valeurstock from totAff.
           
           exec sql
             close curs-b
           end-exec.
           close f-valeurstock.
           goback.



       verImpr.
               if NoPage = 0 then
                   open output f-valeurstock
                   add 1 to NoPage
                   move jour of DateSysteme to jour of LigneEntete
                   move mois of DateSysteme to mois of LigneEntete
                   move annee of DateSysteme to annee of LigneEntete
                   move Heure of HeureSysteme to Heure of LigneEntete
                   move Minute of HeureSysteme to Minute of LigneEntete

                   write e-valeurstock from LigneEntete
                   write e-valeurstock from LigneEntete2
                   write e-valeurstock from LigneEntete3
                   write e-valeurstock from LigneEntete4
                   write e-valeurstock from LigneEntete3
                   perform ImpLigne

               else
                   if (ligneImp = 20)
                       move NoPage to NPage of LigneBasPage
                       write e-valeurstock from LigneBasPage
                       add 1 to nopage
                       move NoPage to NPage of LigneBasPage
                       move 0 to ligneImp
                   end-if
                   perform ImpLigne

               end-if.


       ImpLigne.
           move corresponding article to articleImpr.
           move ref of articleImpr to ref of LigneArticle.
           move stock of articleImpr to stock of LigneArticle.
           move prix of articleImpr to prix of LigneArticle.
           move totU of articleImpr to totU of LigneArticle.
           write e-valeurstock from LigneArticle.
           add 1 to ligneImp.
           
           

           
       end program Program1.
