       program-id. Program1 as "Gestion_Clients.Program1".

       environment division.
       input-output section.
       
              select f-receptioncommande assign to "C:\Users\Quentin\Desktop\receptionCommande.txt"
                     status StatusCode organization Line Sequential access sequential.


              select f-commande assign to "C:\Users\Quentin\Desktop\CMD.txt"
               status StatusCode organization Line Sequential access Sequential.

       data division.
       file section.
       fd f-receptioncommande record varying from 0 to 255.
       01 e-receptioncommande pic x(255).
       
       fd f-commande record varying from 0 to 255.
       01 e-commande pic x(255).
       
       
       working-storage section.
      *D�clarations li�es � SQL
       77 CNXDB STRING.
           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

           EXEC SQL
               INCLUDE SQLDA
           END-EXEC.

      *structure article du fichier recep. commande

       01 DateSysteme.
         10 Annee Pic 99.
         10 Mois Pic 99.
         10 Jour Pic 99.

       77 valeur  pic 9(10).

       77 valeurnum pic 9(6).
       77 valeurAff pic z(6)9.
       77 acommander pic 9(10).
       77 stockminiNum  pic 9(10).
       77 ram pic 9 value 0 .
       77 maxligneetat pic 999.
       77 NbLigne pic 999.
       77 refbdd sql char (10).
       77 varsupp pic 9.

       01 reception_commande.
         10 idArt pic x(36).
         10 qteArt pic 9(6).
         10 nom pic x(30).
         10 prix pic 9(8)v9.

       01 Article.
         10 IdArticle sql char-varying(36).
         10 Ref sql char (10).
         10 Nom sql char-varying (30).
         10 Stock pic 9(10).
         10 StockMini pic 9(10).
         10 Prix pic 9(5)V99.


       01 articleImpr.
         10 IdArticle pic X(36).
         10 Ref pic x(10).
         10 Nom pic x(30).
         10 Stock pic z(9)9.
         10 StockMini pic z(9)9.
         10 Prix pic 9(5)V99.
         
       01 TArticle occurs 255.
         10 idArt pic x(10).
         10 nom pic x(30).
         10 prix pic 9(5)v99.
         10 qteArt pic 9(10).

       01 LigneEcran occurs 16.
         10 IdArticle sql char-varying(36).
         10 Ref sql char (10).
         10 Nom sql char-varying (30).
         10 Stock sql char (10).
         10 StockMini sql char (10).
         10 Prix pic 9(5)V99.

       01 LigneMod occurs 16.
         10 IdArticle sql char-varying(36).
         10 Ref sql char (10).
         10 Nom sql char-varying (30).
         10 Stock sql char (10).
         10 StockMini sql char (10).
         10 Prix pic 9(5)V99.

       01 LigneSupprime Occurs 16.
         10 IdArticle sql char-varying(36).
         10 Ref sql char (10).
         10 Nom sql char-varying (30).
         10 Stock sql char (10).
         10 StockMini sql char (10).
         10 Prix pic 9(5)V99.

      *index tarticle 
       77 tIndex pic 999 value 0.
       77 indexT pic z(2)9.
      *variable interm. utilise dans le perform varying 
       77 ligne pic 999 value 1.

      *Variable diverses.
       77 Option pic X.
       77 OptionArt pic X.
       77 Choix pic x.
       77 RefArtOk pic 9.
       77 NoLine pic 99.
       77 NbLine pic 99.
       77 NbLinetemp pic 99.
       77 LineArt pic 99.
       77 i pic 99.
       77 FinListe pic 9.
       77 AjtArtOk pic 9.
       77 ModArtOk pic 9.
       77 ArtMod pic 9.
       77 MajLigne pic 9.
       77 lignelu pic 9 value 0.
       77 stockint pic 9(6).
       77 fichlu pic 9 value 0.
       77 valeurInt pic 9(5).
       77 stockNum pic 9(10).
       77 ModListe pic 99.
       77 ligImpr pic 9(5).
       77 RefExi pic 9.
       77 OptionValidation pic x.
       77 StatusCode  pic 99.
       77 majbdd pic 9.
      *77 reftab sqlcode 9.
       77 sup pic 9.
       

      *Couleurs du fond de l'ecran.
       77 CouleurFondEcran pic 99 value 15.
      *Couleurs des caracteres.
       77 CouleurCaractere pic 99 value 0.
       77 CouleurMod pic 99.
       77 CouleurSup pic 99.
       77 CouleurPagSui pic 99.
       77 CouleurVal pic 99.





       77 affichageArt pic 99.
       77 Reponse pic x.
       77 temp pic x(99).

       77 EofRecp pic 9.

       77 errDonn pic 9. 

       77 cligne pic 99 value 0.

       77 nb1 pic 9.
       
      *derniere zone du fichier recep. comm. contenant la qteart suivi des espaces 
       77 derzone pic x(50).

      * variable intermediaire 
       77 zone pic x(20).
     

       77 totfichier pic x(6).
       77 nopage pic 999.
       77 lindex pic 99.
       77 ligneImp  pic 999 value 0.

       
      *variable verification total du fichier 
       01 totArtF.
        10 totAtt pic 9(8).
        10 totRecu pic 9(8).
        10 recTot pic 9 value 0.

       01 ligneCourante.
         10 idArt sql char (10).
         10 Nom sql char-varying (30).
         10 prix pic 9(5)v99.
         10 qteArt pic 9(10).

       77 qteArtnum pic 9(6).
       77 qteArtnumAff pic z(5)9.
       
       01 ligneEcranAff.
         10 idArt pic x(10).
         10 nom pic x(30).
         10 qteArt pic z(6)9.
         10 total pic z(11)9.

      *gestion de ligne menu import fichier
       77 linImpof pic 99 value 2.

       01 LigneEntete.
         10 Filler Pic X(23) value "Commande � effectuer : ".
         10 Filler Pic X(52).
         10 Filler Pic X(17) value "Mis � jour le : ".
         10 jour pic 99.
         10 Filler   Pic X value "/".
         10 mois pic 99.
         10 Filler   Pic X value "/".
         10 Annee Pic 99.

       01 LigneColonne.
         10 Filler Pic X(14) Value " Referentiel".
         10 Filler Pic X(20).
         10 Filler Pic X(30) Value "Intitul�".
         10 Filler Pic X(25).
         10 Filler Pic X(20) value "Quantit�".
       

       01 aucunArt.
         10 filler pic x(45).
         10 filler pic x(28) value "Aucune commmande � effectuer".
       
         
       01 articleImprf.
         10 filler pic x.
         10 refArticle pic x(10).
         10 filler pic x(23).
         10 nomArticle pic x(30).
         10 filler pic x(25).
      *  10 stock_min pic 9(10).
      *  10 filler pic x(10).
         10 quantiteImpr pic z(9)9.
           
       01 LigneEntete2.
         10 Filler Pic X(70).

       01 LigneEntete3.
         10 Filler Pic X(120) value all "=".

       01 DernierBasPage.
           10 Filler   Pic X(10) value " --- Page ".
           10 NPage    Pic Z9.
           10 Filler   Pic X.
           10 Filler   Pic X(97) value all "-".

       01 LigneBasPage.
           10 Filler   Pic X(10) value " --- Page ".
           10 NPage    Pic Z9.
           10 Filler   Pic X.
           10 Filler   Pic X(85) value all "-".
           10 Filler   Pic X(13) value " A suivre ---".

       77 titre pic x(50).  
       Screen section.
      *Efface ecran
       01 EffEcr.
         10 line 1 col 1 Blank Screen.

       01 EffLgn.
         10 line 1 col 1 pic x(80) value all " ".

       01 effLgnVar.
         10 line Noline col 1 pic x(80) value all " ".

      *Temp tant que EOL etc ne fonctionne pas....
       01 EffArt.
         10 line 1 Col 1 ERASE EOL.
         10 line 3 Col 1 pic x(80) value all " ".
         10 line 4 Col 1 pic x(80) value all " ".
         10 line 5 Col 1 pic x(80) value all " ".
         10 line 6 Col 1 pic x(80) value all " ".
         10 line 7 Col 1 pic x(80) value all " ".
         10 line 8 Col 1 pic x(80) value all " ".
         10 line 9 Col 1 pic x(80) value all " ".
         10 line 10 Col 1 pic x(80) value all " ".
         10 line 11 Col 1 pic x(80) value all " ".
         10 line 12 Col 1 pic x(80) value all " ".
         10 line 13 Col 1 pic x(80) value all " ".
         10 line 14 Col 1 pic x(80) value all " ".
         10 line 15 Col 1 pic x(80) value all " ".
         10 line 16 Col 1 pic x(80) value all " ".
         10 line 17 Col 1 pic x(80) value all " ".
         10 line 18 Col 1 pic x(80) value all " ".
         10 line 19 Col 1 pic x(80) value all " ".

      *Structure du menu principal.
       01 Menu-Prc background-color is CouleurFondEcran foreground-color is CouleurCaractere.
         10 line 1 col 1 Blank Screen.
         10 line 3 col 32 value " GESTION CLIENTS ".
         10 line 5 col 2 value " Date systeme :".
         10 line 5 col 18 from Jour of DateSysteme.
         10 line 5 col 20 value "/".
         10 line 5 col 21 from Mois of DateSysteme.
         10 line 5 col 23 value "/".
         10 line 5 col 24 from Annee of DateSysteme.
         10 line 5 col 69 value " Option :".
         10 line 5 col 79 pic 9 from Option.
         10 line 8 col 5 value  "- 1 - Gestion Article ............................................ :".
         10 line 9 col 5 value  "- 2 - Importation d'un fichier de commande ....................... :".
         10 line 10 col 5 value "- 3 - Verification de stock ...................................... :".
         10 line 11 col 5 value "- 0 - Fin de traitement .......................................... :".

       01 Menu-Art background-color is CouleurFondEcran foreground-color is CouleurCaractere.
         10 line 1 col 1 Blank Screen.
         10 line 3 col 32 value " GESTION ARTICLE ".
         10 line 5 col 2 value " Date systeme :".
         10 line 5 col 18 from Jour of DateSysteme.
         10 line 5 col 20 value "/".
         10 line 5 col 21 from Mois of DateSysteme.
         10 line 5 col 23 value "/".
         10 line 5 col 24 from Annee of DateSysteme.
         10 line 5 col 69 value " Option :".
         10 line 5 col 79 pic 9 from Choix.
         10 line 8 col 5 value  "- 1 - Recherche Article .......................................... :".
         10 line 9 col 5 value "- 2 - Liste Article .............................................. :".
         10 line 10 col 5 value "- 3 - Ajout Article .............................................. :".
         10 line 12 col 5 value "- 0 - Retour au princpal ......................................... :".

       01 Menu-Ent background-color is CouleurFondEcran foreground-color is CouleurCaractere.
         10 line 1 col 1 Blank Screen.
         10 line 3 col 32 from titre.
         10 line 5 col 2 value " Date systeme :".
         10 line 5 col 18 from Jour of DateSysteme.
         10 line 5 col 20 value "/".
         10 line 5 col 21 from Mois of DateSysteme.
         10 line 5 col 23 value "/".
         10 line 5 col 24 from Annee of DateSysteme.

       01 Ent-vrfS background-color is CouleurFondEcran foreground-color is CouleurCaractere.
         10 line 1 col 1 Blank Screen.
         10 line 2 col 32 value " GENERATION DU FICHIER DE COMMANDE ".
         10 line 3 col 2 value " Date systeme :".
         10 line 3 col 18 from Jour of DateSysteme.
         10 line 3 col 20 value "/".
         10 line 3 col 21 from Mois of DateSysteme.
         10 line 3 col 23 value "/".
         10 line 3 col 24 from Annee of DateSysteme.

       01 Article-T background-color is CouleurCaractere foreground-color is CouleurFondEcran.
         10 line 2 col 1 pic x(80)
         10 Line 2 Col 2 value "No".
         10 Line 2 Col 6 value "REFERENCE". 
         10 Line 2 Col 17 value "Nom".
         10 Line 2 Col 48 value "Prix".
         10 Line 2 Col 57 value "Stock".
         10 Line 2 Col 68 value "Stock Minimal".

       01 Article-L.
         10 Line NoLine Col 2 from NbLine.
         10 Line NoLine Col 6 from Ref of Article.
         10 Line NoLine Col 17 using Nom of Article.
         10 Line NoLine Col 48 using Prix of Article Pic z(4)9V,99.
         10 Line NoLine Col 57 using Stock of Article pic z(9)9.
         10 Line NoLine Col 68 using StockMini of Article pic z(9)9.

       01 Article-L-Ajout.
         10 Line NoLine Col 2 from NbLine.
         10 Line NoLine Col 6 using Ref of Article.
         10 Line NoLine Col 17 using Nom of Article.
         10 Line NoLine Col 48 using Prix of Article Pic z(4)9V,99.
         10 Line NoLine Col 57 using Stock of Article PIC z(9)9.
         10 Line NoLine Col 68 using StockMini of Article pic z(9)9.
         10 Line 6 Col 2 value "[V]alider - [R]etour : ".
         10 Line 6 Col 26 using Reponse.
         
       01 SRchArt background-color is CouleurFondEcran foreground-color is CouleurCaractere.
         10 line 3 col 32 value " Recherche ARTICLE ".
         10 line 5 col 2 value " Date systeme :".
         10 line 5 col 18 from Jour of DateSysteme.
         10 line 5 col 20 value "/".
         10 line 5 col 21 from Mois of DateSysteme.
         10 line 5 col 23 value "/".
         10 line 5 col 24 from Annee of DateSysteme.
         10 line 7 col 2 value " Saisir la reference article ou taper [A] pour annuler".
         10 line 8 col 2 value " Reference article :".

       01 GestionArticle-Options background-color is CouleurFondEcran foreground-color is CouleurCaractere.
      *to do ajout des couleurs pour les champs
         10 line 20 col 1 erase EOS.
         10 line 20 col 1 pic x(80) value all "_".
         10 line 21 col 1 value "-1-Ajout d'un Article ............. :".
         10 line 22 col 1 value "-2-Modification Article, ligne No   :" background-color is CouleurFondEcran foreground-color is CouleurMod. 
         10 line 23 col 1 value "-3-Suppression Article, ligne No    :" background-color is CouleurFondEcran foreground-color is CouleurSup.
         10 line 21 col 39 value "-S-Page Suivante........... :" background-color is CouleurFondEcran foreground-color is CouleurPagSui.
         10 line 22 col 39 value "-A-Annulation ............. :".
         10 line 23 col 39 value "-V-Validation ............. :" background-color is CouleurFondEcran foreground-color is CouleurVal.
         10 line 23 col 69 value "Option :".

           
       procedure division.

      *Menu principal.
           perform Menu.
       Menu.

           perform Menu-Int.
           perform Menu-Trt until Option = 0.
           perform Menu-Fin.



      *Initialisation du menu principal.
       Menu-int.

      *Initialisation des variables 
       

      *Initialisation de "Option" � "1" pour rentrer au moins une fois dans Menu-Trt.
           Move 1 to Option.

      *R�cup�ration de la date du syst�me(format AAMMJJ)
           accept DateSysteme from date.

      *Connexion � la base de donn�es.
           
      *Pour soundouce : "Trusted_Connection=yes;Database=GestionClients;server=      ?       ;factory=System.Data.SqlClient;" 
      *Pour quentin : "Trusted_Connection=yes;Database=GestionClients;server=DESKTOP-HQ-A-W\SQLEXPRESS;factory=System.Data.SqlClient;"
           MOVE
             "Trusted_Connection=yes;Database=GestionClients;server=DESKTOP-HQ-A-W\SQLEXPRESS;factory=System.Data.SqlClient;"
             to cnxDb.
           exec sql
               connect using :cnxDb
           end-exec.

           if (sqlcode not equal 0) then
               stop run
           end-if.

           exec sql
               SET AUTOCOMMIT ON
           end-exec.
      *Execution du menu principal.
       Menu-Trt.

      *Mise � "0" de "Option" car c'est l'option par d�fault qui met fin a la boucle.
           Move ' ' to Option.
      *Affichage du Menu Principal.
           Display Menu-Prc.
      *Recuperation de la saisie utilisateur.
           Accept Option line 5 col 79.
      *Test de la valeur de "Option" le choix de l'utilisateur, on declanche le traitement approprie.
           evaluate Option
               when 1
                   perform MenuGstArt
               when 2
                   perform LecFic
               when 3
                   perform VerSto
           end-evaluate.

      *Fin du menu principal.
       Menu-Fin.
           goback.

      *Menu Gestion Article
       MenuGstArt.

           perform MenuGstArt-init.
           perform MenuGstArt-trt until choix = 0.
           perform MenuGstArt-fin.

       MenuGstArt-init.
           Move 1 to Choix.
      *En fonction du choix de l'utilisateur Recherche,Consultation,Ajout  Article
       MenuGstArt-trt.
           Move ' ' to Choix.
           Display Menu-Art.
           accept Choix line 5 col 79.
           evaluate Choix
               when 1
                   perform RchArt
                   move 0 to ARtMod
               when 2
                   perform ListArt
                   move 0 to ARtMod
               when 3 
                   perform AjtArt
                   move 0 to ArtMod
           end-evaluate.
       MenuGstArt-fin.

      *Recherche d'un article
       RchArt.
           perform RchArt-init.
           perform RchArt-trt until RefArtOk = 1.
           perform RchArt-fin.

       RchArt-init.
           move 0 to RefArtOk.
           move "" to Ref of Article.
           display effecr.
       RchArt-trt.
           display EffEcr.
           Display SRchArt.
           Accept ref of Article line 8 col 24.

      * si "A"/"a" alors fin et retour au menu pr�c�dent sinon perform listart
           if ref of article = "a" then
               move "A" to ref of Article
               move 1 to RefArtOk
           else if ref of article <> ""
               perform ListArt
           end-if.

       RchArt-fin.

      *Affichage de la liste des articles en fonction du choix recherche ou consultation
       ListArt.
           perform ListArt-init.
           perform ListArt-trt until affichageArt = 1.
           perform ListArt-fin.

       ListArt-init.
           move 0 to affichageArt.
           move 3 to NoLine.
           move 0 to NbLine.
           move 0 to FinListe.
           move 0 to ModListe.
           move 1 to RefArtOk.

           perform ClearTab.


           display EffEcr.
           display Article-T.

      *Si c'est une recherche (choix 1) ou une consultation (Choix 2).
           if (choix = 2) then
               exec sql
                   declare C-Article-Liste cursor for
                       select id,ref,nom,stock,stock_min,prix
                       from stock
                       order by ref
               end-exec

               exec sql
                   open C-Article-Liste
               End-exec
           else
               if (choix = 1) then
                   exec sql
                       declare C-Article-Rch cursor for
                           select id,ref,nom,stock,stock_min,prix
                           from stock
                           where ref = :article.ref
                       order by ref
               end-exec

               exec sql
                   open C-Article-Rch
               End-exec
           end-if.
           
       ListArt-trt.

           if (choix = 2) then
               exec sql
                   fetch C-Article-Liste into :Article.idarticle, :Article.Ref,:Article.nom,:Article.stock,:Article.stockmini,:Article.prix
               End-exec
           else
           if (choix = 1) then
               exec sql
                   fetch C-Article-Rch into :Article.idarticle, :Article.Ref,:Article.nom,:Article.stock,:Article.stockmini,:Article.prix
               End-exec
           end-if.

           if SQLCODE = 0 or SQLCODE = 1 then
               perform AffichageLigneArt
      *Fin de liste
           else if SQLCODE = 100
      *        Si c'est une recherche et que l'article n'est pas dans la base de donn�e alors on demande a l'utilisateur si il veut le cr�e puis redirection sinon retour
               if (Choix = 1 and NbLine = 0)
                   display EffEcr
                   move 1 to affichageArt
                   move 0 to RefArtOk
                   display "Article non trouve, le cree ? [O]ui [N]on : " line 1 col 1
                   accept reponse line 1 col 46
                   if reponse = 'o' then
                       move 'O' to reponse
                   end-if
                   if reponse = 'n' then
                       move 'N' to reponse
                   end-if
                   if reponse = 'O' then
                       perform AjtArt
                   else if reponse = 'N' then
                       display EffLgn
                   end-if
      *        Sinon c'est une consultation on notifie a l'utlisateur la fin de la liste et perform ArticleOption
               else
                   display "Fin de la liste" line 1 col 1
                   move 1 to FinListe
                   perform ArticleOption
               end-if
           end-if.
       ListArt-fin.
           if (choix = 2) then
               exec sql
                   close C-Article-Liste
               End-exec
           else if (choix = 1 ) then
               exec sql
                   close C-Article-Rch
               end-exec
           end-if.

      *Affichge d'une ligne d'un article
       AffichageLigneArt.
           ADD 1 TO NOLINE.
           ADD 1 TO NbLine.

           move corresponding article to LigneEcran(NbLine).
           display Article-L.

      *    Si en bas de page alors on perform articleoption 
           if (NBLine = 16) then
               perform ArticleOption
           end-if.

          
      * Permet a l'utilsiateur de choisir une option � effectuer sur les articles affich�s
       ArticleOption.

           perform ArticleOption-init.
           perform ArticleOption-trt until OptionArt = "A" or OptionArt = "S" or OptionArt = "V"
           perform ArticleOption-fin.

       ArticleOption-init.
           move " " to OptionArt.
       ArticleOption-trt.
           move " " to OptionValidation
      *On test pour savoir si une option doit �tre disponible, sinon on la cache
           if FinListe = 1 then
               move CouleurFondEcran to CouleurPagSui
           else
               move CouleurCaractere to CouleurPagSui
           end-if.

           if Nbline < 1 then
               move CouleurFondEcran to CouleurMod
               move CouleurFondEcran to CouleurSup
           else
               move CouleurCaractere to CouleurMod
               move CouleurCaractere to CouleurSup
           end-if.

           if ((ModListe > 0) or (sup > 0)) then
               move CouleurCaractere to CouleurVal
           else
               move CouleurFondEcran to CouleurVal
           end-if.

           display GestionArticle-Options.
           accept OptionArt line 23 col 78.
           display EffLgn.

           if  OptionArt = 'a'
               move "A" to OptionArt.
           if OptionArt = "v"
               move "V" to OptionArt.
           if OptionArt = "s"
               move "S" to OptionArt.

      *En fonction la saisi de l'utilisateur on regarde si l'option est bien possible et on perform en fonction
           evaluate OptionArt
               when 1
                   if ModListe > 0 or sup > 0 then
                       display "[V]alidation ou [A]nnulation des changements avant l'ajout d'un article." line 1 col 1
                   else
                       move "A" to OptionArt
                       move 1 to affichageArt
                       perform AjtArt
                   end-if
               when 2
                   if Nbline > 0 then
                       if sup = 0 then
                           perform ModArt
                       else
                           display EffLgn
                           display "[V]alidation / [A]nnulation des suppresions avant la modification d'un article" line 1 col 1
                       end-if
                   end-if
               when 3
                   if Nbline > 0 then
                       if ModListe = 0 then
                           perform SupArt
                       else
                           display EffLgn
                           display "[V]alidation / [A]nnulation des modifications avant la modification d'un article" line 1 col 1
                       end-if
                   end-if
               when "V"
                   if ModListe = 2 then
                       perform MiseAJourBDD
                       move 0 to ModListe
                       move " " to OptionArt
                   else
                       if sup = 1 then
                           perform suppVald
                           move 0 to sup
                           move 1 to affichageArt

                       end-if
               when "S"
      *            S'il reste des donn�es � afficher
                   if (FinListe = 0) then
      *                s'il y a eu des modifications on demande s'il faut valider les changements ou non avant de passer � la page suivante
                       if modliste = 2 then
                           display efflgn
                           display "[V]alider ou [A]nnuler les changements :" col 1 line 1
                           accept OptionValidation line 1 col 42
                           display EFFLGN
                           if OptionValidation = 'a'
                               move "A" to OptionValidation
                           end-if
                           if OptionValidation = "v"
                               move "V" to OptionValidation
                           end-if
                           evaluate OptionValidation
                               when "V"
                                   perform MiseAJourBDD
                                   display EffArt
                               when "A"
                                   display Efflgn
                                   move " " to OptionArt
                           end-evaluate

                       else
      *                    S'il y a eu des suppressions on demande s'il faut les validers ou non
                           if sup = 1 then
                               display efflgn
                               display "[V]alider ou [A]nnuler les suppressions :" col 1 line 1
                               accept OptionValidation line 1 col 45
                               display efflgn
                               if OptionValidation = 'a'
                                   move "A" to OptionValidation
                               end-if
                               if OptionValidation = "v"
                                   move "V" to OptionValidation
                               end-if
                               evaluate OptionValidation
                                   when "V"
                                       perform suppVald
                                       display EffArt
                                   when "A"
                                       display Efflgn
                                       move " " to OptionArt
                               end-evaluate
                           else
                               display EffArt
                           end-if
                       end-if
                   else
                       move " " to OptionArt
                   end-if
               when "A"
                   move 1 to affichageArt

           end-evaluate.

     
       ArticleOption-fin.
           move 3 to noLine.
           move 0 to nbline.
           move 0 to sup.
           move 0 to ModListe.

      *Nettoie les tableaux
       ClearTab.
           PERFORM VARYING i FROM 1 BY 1 UNTIL i = 17
               initialize LigneEcran(i) Replacing Numeric Data By 0 Alphanumeric Data By " "
               initialize LigneMod(i) Replacing Numeric Data By 0 Alphanumeric Data By " "
               initialize LigneSupprime(i) Replacing Numeric Data By 0 Alphanumeric Data By " "
           END-PERFORM.

      *Ajout d'un article
       AjtArt.
           move 3 to NoLine.
           move 0 to NbLine.

           perform AjtArt-init.
           perform AjtArt-trt until AjtArtOk = 1.
           perform AjtArt-fin.

       AjtArt-init.
           move 0 to AjtArtOk.
           add 1 to NoLine.
           add 1 to NbLine.

      *Initialisation de valeur par default pour Article.
           Move 0 to ref of Article.
           Move "Nom" to nom of Article.
           Move 0 to Prix of Article.
           Move 0 To Stock of Article.
           Move 0 To StockMini of Article.
           
           display EffEcr.
       AjtArt-trt.
           move "" to Reponse.

           display Article-T.
           display Article-L-ajout.
           accept Article-L-ajout.

           if reponse = "v"
               move "V" to reponse.

           if reponse = "r"
               move "R" to reponse
           end-if.
      *Si "R"/"r" Alors retour sinon on regarde si l'article existe deja dans la base, si les champs ne sont pas nul et si c'est bon alors on perform MiseAjourBDD sinon on renseigne l'utilisateur
           if reponse = "R" then
               move 1 to AjtArtOk
           end-if.
           if reponse = "V" then 
                   exec sql
                     Select Nom into :TEMP
                     from stock
                     where Ref = :Article.ref
           end-exec
               if (Ref of Article <> "") and (SQLCODE = 0 or SQLCODE = 1) then
                   Display EffEcr
                   Display "Reference deja presente" Line 1 Col 1
               else
                   if (Ref of Article <> "") and (Nom of Article <> "") and (Prix of Article <> "") and (Stock of Article <> "") and (StockMini of Article <> "")
                       move corresponding article to LigneMod(NbLine)
                       move 1 to modliste
                       perform MiseAJourBDD
                   else
                       Display EffEcr
                       Display "Veuillez renseigner chaque champ" Line 1 Col 1
                   end-if
               end-if
           end-if.
       AjtArt-fin.

      *Modification d'article
       ModArt.
           Move 0 to LineArt.
      *On v�rifie si il y a au minimum une ligne et si l'utilisateur a saisi un ligne existante pour la modification
           if NbLine > 0 then
               accept lineArt line 22 col 34
               if LineArt > 0 and LineArt <= NbLine then
                   perform MiseAJourLigne
               end-if
           end-if.
       MiseAJourLigne.
           perform MiseAJourLigne-Init.
           perform MiseAJourLigne-Trt until MajLigne = 1.
           perform MiseAJourLigne-Fin.

       MiseAjourLigne-init.
           move 0 to MajLigne.
           move 3 to NoLine.
           move nbline to NbLinetemp.
           Add LineArt to NoLine.
           move LineArt to NbLine.
           
       MiseAjourLigne-trt.
           move 0 to RefExi.
           Move corresponding LigneEcran(LineArt) to Article.
      *Si il y a une une modification au mini, alors on test le tableau des mod,
      *    si la ligne choisie est dedans, on la met dans article pour l'afficher,
      *    sinon on prend la ligne de base.
           if ModListe = 2 then
               PERFORM VARYING i FROM 1 BY 1 UNTIL i = 17
                   if ref of article equal ref of LigneMod(i) then
                       move 1 to RefExi
                       if refexi = 1 then
                           move corresponding LigneMod(i) to Article
                       end-if
                   end-if
               END-PERFORM
           end-if.
           Display Article-L.
           accept Article-L.

      *Si la r�f�rence n'est pas pr�sente dans la bdd alors on met fin aux boucles et indique qu'une modification est faite.
           if ((Nom of Article <> "") and (Prix of Article <> "") and (Stock of Article <> "") and (StockMini of Article <> ""))
               Display efflgn
      *On ajoute 1 au compteur d'article modifier
               add 1 to ArtMod
      *On met ModListe � 1 car il y a eu une modification
               move 2 to ModListe
      *On stock la ligne mod dans le tableau des modifications
               move corresponding Article to LigneMod(LineArt)
      *On met MaJLigne � 1 pour sortir de la boucle de MiseAjourLigne-trt
               move 1 to MajLigne
           end-if.

       MiseAjourLigne-fin.
           move nblinetemp to NbLine.


      *Suppresion Article
       SupArt.
        Move 0 to LineArt.
           if NbLine > 0 then
               accept lineArt line 23 col 34
               if LineArt > 0 and LineArt <= NbLine then
                   perform supARtligne
               end-if
           end-if.


       supARtligne.
       move corresponding LigneEcran(LineArt) to LigneSupprime(LineArt).
       if (ref of ligneEcran(lineArt) not = "") then
      *    move ligneEcran(lineArt) to LigneMod(lineArt)
           move "" to  ref of ligneEcran(LineArt)
           move "" to nom of ligneEcran(LineArt)
           move 0 to prix of ligneEcran(LineArt)
           move 0 to stock of ligneEcran(LineArt)
           move 0 to stockmini of ligneEcran(LineArt)

           move 3 to NoLine
           move nbline to NbLinetemp
           Add LineArt to NoLine
           move LineArt to NbLine
           display effLgnVar
           move corresponding LigneEcran(LineArt) to Article
           Display Article-L
           move NbLinetemp to nbline
           move 1 to sup
       end-if.

       suppVald.
        perform varying Ligne from 1 by 1 until Ligne = 17
               move corresponding LigneSupprime(ligne) to article
               if (nom of article not = "")
               exec sql 
               delete from stock where 
               ref = :article.ref
               end-exec

               if (sqlcode not = 0)
                   display "Erreur delete" line 10
               end-if
               end-if
               initialize LigneSupprime(ligne) Replacing Numeric Data By 0 Alphanumeric Data By " "
               
           end-perform.
           move 1 to varsupp.


      *Conversation avec la base, ajout/modification
       MiseAJourBDD.
      *Modliste vaut 1 si c'est un ajout et 2 si c'est une modification
       evaluate modliste 
           when 1 perform AjoutBDD
           when 2 perform UpdateBDD
       end-evaluate.


       AjoutBDD.
           perform varying ligne from 1 by 1 until ligne = 17
            move corresponding LigneMod(ligne) to article
            if(ref of article not = "")
                 exec sql 
                   insert into stock 
                   (id,ref,nom,prix,stock,stock_min)
                   VALUES
                   (newid(), :article.Ref, :Article.nom , :Article.prix ,:Article.stock ,:article.StockMini )
               end-exec
            
               if(sqlcode not = 0 )
                   display effecr
                   move 1 to majbdd
                   display " ERREUR INSERT "  line 5 col 3 
                   accept reponse              end-if
           end-if 
          end-perform.
 

       UpdateBDD.

           perform varying ligne from 1 by 1 until ligne = 17
               move LigneMod(ligne) to article
               move ref of LigneEcran(ligne) to refbdd
               if (ref of article not = "")
                   exec sql
                       update stock
                       set ref = :article.ref, 
                       nom = :article.nom,
                       prix = :article.prix, 
                       stock = :article.stock,
                       stock_min = :article.StockMini
                       where ref = :refbdd
               end-exec
            
               if(sqlcode not = 0 )
                   display efflgn
                   move 1 to majbdd
                   display " ERREUR update "  line 1 col 1
               end-if
            end-if

           end-perform.

      

      *Execution de lecture du fichier.
       LecFic.
           perform lectureFichier-Init.
           perform lectureFichier-Trt until eofRecp = 1.
           perform lectureFichierFin.
           

       lecturefichier-init.
           INITIALIZE TArticle.
           move 0 to qteArtnum.
           move "" to derzone.
           move "" to zone.
           move 0 to EofRecp.
           move "" to idArt of reception_commande.
           move 0 to qteArt of reception_commande.
           move "" to nom of reception_commande.
           move 0 to prix of reception_commande. 
           move 0 to idArt of ligneEcranAff.
           move "" to nom of ligneEcranAff.
           move 0 to qteArt of ligneEcranAff.
           move 0 to total of ligneEcranAff.
           move 0 to valeurnum.
           move 0 to valeurAff.
           move 0 to totAtt of totArtF.
           move 0 to tIndex.
           move 0 to errDonn.
           move " " to zone.
           move 0 to totfichier.
           move 0 to totRecu of totArtF.
           move " " to idArt of reception_commande.
           move 0 to qteArt of reception_commande.
           move 7 to linImpof.
           move 0 to eofRecp.
           move 0 to rectot.
           
           display EffEcr.
           move " RECEPTION DE COMMANDE " to titre.
           display Menu-Ent.
           open input f-receptioncommande.
           
           if(StatusCode not = 0)
               display " Erreur fichier  " line ligImpr
               move 1 to eofRecp
               accept reponse
           end-if.

       lecturefichier-trt.
           read f-receptioncommande
               at end
                   perform affichFichier
                   move 1 to eofRecp
               not at end
                   perform extdonn
           end-read.
       lectureFichierFin.
           close f-receptioncommande.

           

      * extraction des donnees du fichier
       extdonn.
      *recuperation total pour le verifier
           if (rectot of totArtF = 0)
               unstring e-receptioncommande delimited by ':' into
                 zone,
                 totfichier
               end-unstring
               unstring totfichier delimited by " " into
                 totRecu of totArtF
               end-unstring
               move 1 to recTot of totArtF

           else

               unstring e-receptioncommande delimited by ';' into
                 idArt of reception_commande,
                 nom of reception_commande,
                 prix of reception_commande,
                 derzone
               end-unstring

      * extraction de la quantite de la zone derzone
               unstring derzone delimited by " " into
                 qteArt of reception_commande
               end-unstring

               add qteArt of reception_commande to totAtt of totArtF

      * verification idart & quantite
               if (totRecu of totArtF < 0)
                   move 0 to errDonn
                   perform errExtDonn
                   accept reponse
               end-if

               if (idArt of reception_commande = space)
                   move 1 to errDonn
                   perform errExtDonn
                   accept reponse
               end-if

               if (fichlu = 0 and (qteArt of reception_commande equal 0 or qteArt of reception_commande < 0))
                   move 2 to errDonn
                   perform errExtDonn
                   accept reponse
               end-if
                if (prix of reception_commande < 0)
                   move 4 to errDonn
                   perform errExtDonn
                   accept reponse
               end-if
              
               add 1 to tIndex

               move idart of reception_commande to idart of TArticle(tindex)
               move nom of reception_commande to nom of TArticle(tindex)
               move prix of reception_commande to prix of TArticle(tindex)
               move qteArt of reception_commande to qteArt of TArticle(tindex)
               

           end-if.

       affichFichier.
           if ((totAtt of totArtF not = totRecu of totArtF) and lignelu =0)
               move 3 to errDonn
               perform errExtDonn
           else
               
      
           move 5 to linImpof
           move totRecu of totArtF to total of ligneEcranAff
           add 2 to linImpof
           display 'Le fichier a bien ete importe' line linimpof col 3
           add 3 to linImpof
           display 'Total nouveau article : ' line linImpof col 3
           display total line linImpof col 25
           add 2 to linImpof 
      * ici afficher maj des articles faire somme des fichiers          
           display 'Lignes ajoutees : ' line linImpof col 3
           move tindex to indexT
           display indexT line linImpof col 24
           add 1 to linImpof
          


           perform varying ligne from 1 by 1 until ligne > tIndex
               move TArticle(ligne) to ligneCourante
               move idArt of ligneCourante to idArt of ligneEcranAff
               move nom of ligneCourante to nom of ligneEcranAff
               perform affmaj
               move qteArt of ligneCourante to qteArt of ligneEcranAff

           end-perform



           if (tindex < 5)
           display "Mise a jour des articles : " line linImpof col 3
           add 2 to linImpof
           display "Ref" col 2 line linImpof
           display "Intitule" col 20 line linImpof
           display "Ancien stock" col 38 line linImpof
           display "Ajout" col 56 line linImpof
           display "Nouveau stock" col 64 line linImpof
           add 1 to linImpof


      *boucle sur le tableau contenant les lignes du fichier 
      *appelle affmaj qui lance les requetes sql 
           perform varying ligne from 1 by 1 until ligne > tIndex
               move TArticle(ligne) to ligneCourante
               move idArt of ligneCourante to idArt of ligneEcranAff
               move nom of ligneCourante to nom of ligneEcranAff
               perform affmaj
               move qteArt of ligneCourante to qteArt of ligneEcranAff
               display idArt of ligneCourante line linImpof col 2
               display nom of ligneEcranAff line linImpof col 20
               move valeurnum to valeurAff
               display valeurAff line linImpof col 38
               display qteArt of ligneEcranAff line linImpof col 53
               move qteartnum to qteartnumaff
               display qteartnumaff line linimpof col 64
               add 1 to linImpof

           end-perform
           end-if.    
           move 1 to fichlu.
            display 'Taper entrer pour retourner au menu ' line 24 col 3
           accept reponse.
           
      
       errExtDonn.
           move 7 to linImpof.
           display "Erreur dans le fichier reception de commande : " line linImpof col 3
           add 2 to linImpof.
           evaluate errDonn
               when 0 
                   display 'Erreur totale recu' line linImpof col 3
               when 1
                   display 'Erreur mise en forme du fichier' line linImpof col 3
               when 2
                   display 'Erreur quantite recue' line linImpof col 3
               when 3
                   display 'Total incorrect :' line linImpof col 3
                   display totRecu of totArtF line linImpof col 20
                   add 1 to linImpof
                   display 'Au lieu de : ' line linImpof col 3
                   display totAtt of totArtF line linImpof col 20
               when 4 
                   display 'Prix incorrect' line linImpof col 3 
                   add 1 to linimpof
                   display prix of reception_commande linimpof col 3 
           end-evaluate.
           add 3 to linimpof.
           display " Fichier non accepte. La base de donnees n'a pas ete modifie." line linImpof col 3.
           accept reponse.
                  



       
      *Execution de la verification du stock.
      *Comparaison du stock au stock minimal via interogation SQL  
       VerSto.
        perform VerifStock-init.
        perform VerifStock-trt until ram =0.
        perform VerifStock-fin.

       VerifStock-init .
       move 1 to ram.
        exec sql
           declare C-articleImp cursor for
              select ref,nom,stock,stock_min
              from stock
              where stock < stock_min
              order by ref
        end-exec.

        exec sql
           open C-articleImp
        End-exec.

        Move 0 to NoPage.
        add 1 to MaxLigneEtat giving NbLigne.

        
       VerifStock-trt.
        exec sql
           fetch C-articleImp into :Article.Ref,:Article.nom,:Article.stock,:Article.stockmini
        End-exec.
           if (sqlcode equal 0 or SQLCODE equal 1) then
               perform verImpr
               move 1 to nb1
           else if (sqlcode = 100 and nb1 = 0)
               display EffEcr
               move "Aucune commande a effectuer  " to titre
               display Menu-Ent
               move 0 to ram
               accept reponse

           else if (sqlcode = 100 and nb1 not = 0)
           display EffEcr
               move "Le fichier a ete cree " to titre
               display Menu-Ent
               move 0 to ram
               accept reponse
           end-if.



       VerifStock-fin.
        exec sql
          close C-articleImp
        end-exec.
         if NoPage > 0 then
           move NoPage to NPage of DernierBasPage
           write e-commande from DernierBasPage
           close f-commande
         end-if.


       verImpr.
           if NbLigne > MaxLigneEtat then
               if NoPage = 0 then
                   open output f-commande
                   add 1 to NoPage
                   move jour of DateSysteme to jour of LigneEntete
                   move mois of DateSysteme to mois of LigneEntete
                   move annee of DateSysteme to annee of LigneEntete
                   write e-commande from LigneEntete
                   write e-commande from LigneEntete2
                   write e-commande from LigneEntete3
                   write e-commande from LigneColonne
                   perform ImpLigne 
                
               else
                  if (ligneImp = 20)
                     move NoPage to NPage of LigneBasPage
                     write e-commande from LigneBasPage 
                     add 1 to nopage
                     move NoPage to NPage of LigneBasPage
                     move 0 to ligneImp
                  end-if
                  perform ImpLigne 

               end-if

           end-if.
               


      *Impression de ligne dans le fichier de commande
       ImpLigne.
           move corresponding article to articleImpr.
           move ref of articleImpr to refArticle of articleImprf.
           move nom of articleImpr to nomArticle of articleImprf.
           move stock of articleImpr to stockNum.
           move stockmini of articleImpr to stockminiNum.
           subtract stockminiNum from stockNum giving acommander.
           move acommander to quantiteImpr of articleImprf.
           write e-commande from articleImprf.
           add 1 to ligneImp.



      *Mise a jour de la base de donnees en fonction de la commande receptionne 
      *Si l article n'existe pas alors il est cree avec un 0 comme valeur par defaut dans stock_min 
       affmaj.
       exec sql 
           select stock into :valeur  
           from stock 
           where ref = :ligneCourante.idart
       end-exec .
       if (sqlcode equal 100 ) then
             exec sql 
             insert into stock
               (id, 
               ref,
               nom,
               stock,
               stock_min,
               prix)
               VALUES
               (newid(),
                :ligneCourante.idArt,
                :ligneCourante.nom,
                :ligneCourante.qteArt,
                0,
                :ligneCourante.prix)
             end-exec 
           if (sqlcode not equal 0) then
               display " erreur sql insert" line 30
               accept reponse
           end-if
           
       else if (sqlcode = 0 or sqlcode= 1)
          
      *il faut mettre a jour cette ligne     
           
      *** valeur correspond a lancien stock 
      * valeurnum = valeur que je peux add 
               move valeur to valeurnum
               move qteArt of ligneCourante to qteArtnum

      * la ligne n'a pas ete mis a jour (pour condition d'affichage)
               move 0 to lignelu
           
      * calcul du nouveau stock     
               add valeurnum to qteartnum
     
      *mise a jour du stock avec qteartnum 
               exec sql
                   update stock
                   set stock = :qteartnum
                   where ref = :ligneCourante.idArt
               end-exec 
      
               if (sqlcode not equal 0) then
                   display " erreur sql update" line  18
                   accept reponse
               end-if
       
       end-if.
       end program Program1.